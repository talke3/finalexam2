import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CityService } from '../city.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { City } from '../interfaces/city';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { PredictService } from '../predict.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {

  cities$; 
  cities:any[] = [{cityName:'London'}, {cityName:'Paris'}, {cityName:'Tel Aviv'}, {cityName:'Jerusalem'}, {cityName:'Berlin'}, {cityName:'Rome'}, {cityName:'Dubai'}, {cityName:'Athens'}];
  cityId:string; 
  city:string; 
  cityName:string;
  temperature:number; 
  image:string; 
  country:string; 
  humidity:number;
  result:string;
  weatherData$:Observable<City>;
  hasError:Boolean = false;
  errorMessage:string;
  dataButton:boolean = true;
  predictButton:boolean = true;
  saveButton:boolean = true;
  
  

  getData(cityName:string):void{
    this.cities$ = this.cityService.searchWeatherData(cityName);
    let cityObj = this.cities.find(city =>city.cityName === cityName);
    this.cities$.subscribe(
      data => {
        cityObj.temperature = data.temperature;
        cityObj.image = data.image;
        cityObj.humidity = data.humidity;
      }
    )
    this.dataButton = false;
    console.log({cityObj})
  }

  addCity(){
    this.cityService.addCity(this.temperature, this.humidity, this.result,this.image);
    this.saveButton = false;
    }
  

  predict(){
    this.predictService.predict(this.temperature, this.humidity).subscribe(
      res => { 
        console.log(res);
        if(res < 50){
          var result = 'Will not rain';
        } else {
          var result = 'Will rain'
        }
        this.result = result;
        this.predictButton = false;
        console.log(result);}
    );  
  }

  constructor(private cityService:CityService,private route:ActivatedRoute, public authService:AuthService,private db:AngularFirestore,private predictService:PredictService) { }

  ngOnInit(): void {
    this.cities$ = this.cityService.getCity(); 
    this.cities$.subscribe(
      docs =>{
        console.log('worked');           
        this.cities = [];
        for(let document of docs){
          const city:City = document.payload.doc.data();
          city.id = document.payload.doc.id; 
          this.cities.push(city); }
     }
     )
     
     this.city = this.route.snapshot.params.city; 
     this.weatherData$ = this.cityService.searchWeatherData(this.city); 
     this.weatherData$.subscribe(
       data => {
         this.temperature = data.temperature;
         this.image = data.image;
         this.humidity = data.humidity; 
       }, 
       error =>{
         console.log(error.message);
         this.hasError = true;
         this.errorMessage = error.message; 
       }
     )
   }

}
