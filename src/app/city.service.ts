import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { City } from './interfaces/city';
import { WeatherRaw } from './interfaces/weather-raw';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  cityCollection:AngularFirestoreCollection  = this.db.collection(`/cities`); ;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  private URL = "https://api.openweathermap.org/data/2.5/weather?q="; 
  private KEY = "2b0d3a2c471e1176ccea8b018bd5731c"; 
  private IMP = "units=metric"; 
  constructor(private db:AngularFirestore, public router:Router, private http:HttpClient) { }

  public getCity(){
    return this.cityCollection.snapshotChanges()      
  } 

  predict(temperature:number, humidity:number){
    let json = {
      "data":
      {
      "temperature": temperature,
      "humidity": humidity,
      }
    }
  }

  addCity(temperature:number,humidity:number,result:string,image:string){
    const city = {temperature:temperature,humidity:humidity,result:result,image:image}; 
    this.cityCollection.add(city);
    this.router.navigate(['/welcome']);
  }
 

  searchWeatherData(cityName:string):Observable<City>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }
  
  
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server erorr'); 
  }
  
  private transformWeatherData(data:WeatherRaw):City{
    return {
      id:data.main.id,
      name:data.name,
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      humidity:data.main.humidity
      
    } 
  }
}






