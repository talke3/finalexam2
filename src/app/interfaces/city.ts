export interface City {
    id:string,
    name:string,
    country:string,
    image:string,
    description:string,
    temperature:number,
    humidity:number,
    result?: string
}
