export interface WeatherRaw {
    
    weather:[
       {
          description:string
          icon:string
       }
    ],
    main:{
       id:string,
       temp:number,
       humidity:number
    },
    sys:{
       country:string,
    },
    name:string,

 }