// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA0y6rfv0OqHI92HAd7lTwfh5up-ST-sOE",
    authDomain: "finalexam2-4d424.firebaseapp.com",
    projectId: "finalexam2-4d424",
    storageBucket: "finalexam2-4d424.appspot.com",
    messagingSenderId: "292372372708",
    appId: "1:292372372708:web:5b33fa94ae0c7282f5e0f1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
